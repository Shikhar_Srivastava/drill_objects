function invert(obj){
    var returnObj = {};
    for(var key in obj){
      returnObj[obj[key]] = key;
    }
    return returnObj;
  }

module.exports = invert;