function mapObject(obj, cb) {
    let newObj = {}
    
    if(typeof obj !== 'object')
        return {};
    for(let keys in obj){
        newObj[keys]= cb(keys, obj)
    }
    return newObj;
}

module.exports = mapObject;